<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Section;
use App\Models\Question;
use Illuminate\Http\Request;


class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::paginate(20);
       return view('dashboard.question.index', [
           'questions' => $questions
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::get()->all();
        $quizess = Quiz::get()->all();
        return view('dashboard.question.create', [
            'sections' => $sections,
            'quizess' => $quizess
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        //validation
        $request->validate([
            'title' => 'required|max:255',
            'type' => [function ($attribute, $value, $fail) {
                if( $value === 'single' || $value === 'multiple') {
                    return true;
                } else {
                    $fail($attribute.' is invalid.');
                }
            }],
            'quiz_id' => 'required|integer|gt:0',
            'section_id' => 'required|integer|gt:0'
        ]);
        //create object and save
        $question = Question::create([
            'quiz_id' => $request->quiz_id,
            'section_id' => $request->section_id,
            'title' => $request->title,
            'type' => $request->type,
            'options' => json_encode($request->options),
            'answer' => json_encode($request->answer),
        ]);
        //set flash & redirect
        if($question){
            session()->flash('success', 'Question is successfully created!'); 
            return redirect(route('question.index'));
        }
        else{
            session()->flash('error', 'Failed to create question!'); 
            return redirect(route('question.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return view('dashboard.question.show', [
            'question' => $question
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $sections = Section::get()->all();
        $quizess = Quiz::get()->all();
        return view('dashboard.question.edit', [
            'sections' => $sections,
            'quizess' => $quizess,
            'question' => $question,
            'options' => (json_decode($question->options) == null)? [] : json_decode($question->options),
            'answer' => (json_decode($question->answer) == null)? [] : json_decode($question->answer)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        // dd($request->all());
        //validation
        $request->validate([
            'title' => 'required|max:255',
            'type' => [function ($attribute, $value, $fail) {
                if( $value === 'single' || $value === 'multiple') {
                    return true;
                } else {
                    $fail($attribute.' is invalid.');
                }
            }],
            'quiz_id' => 'required|integer|gt:0',
            'section_id' => 'required|integer|gt:0'
        ]);
        //update object and save
        $question->update([
            'quiz_id' => $request->quiz_id,
            'section_id' => $request->section_id,
            'title' => $request->title,
            'type' => $request->type,
            'options' => json_encode($request->options),
            'answer' => json_encode($request->answer),
        ]);

        //set flash & redirect
        if($question){
            session()->flash('success', 'Question is successfully updated!'); 
            return redirect(route('question.show', $question->id));
        }
        else{
            session()->flash('error', 'Failed to update question!'); 
            return redirect(route('question.edit', $question->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //delete question
        $question->delete();
        //set flash message
        session()->flash('success', 'Question is successfully deleted!'); 
        //redirect
        return redirect(route('question.index'));
    }
}
