<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Section;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Get list of the quizess from API request
     *
     * @return \Illuminate\Http\Response
     */

    public function list(){
        $quizess = Quiz::get()->all();
        $list = [];
        foreach ($quizess as $key => $quiz) {
           $values = $quiz->questions->groupBy('section_id')->toArray();
           $totalQuestions = $quiz->questions->count();
           $data = [];
          
           $sectionData = [];
           foreach ($values as $section_id => $value) {
               $section = Section::where('id', $section_id)->select('title')->first();
               $questionData = [];
               foreach($value as $question) {
                   $singleQuestion = [
                       'title' => $question['title'],
                       'options' => json_decode($question['options']),
                       'answer' => json_decode($question['answer']),
                       'type' => $question['type']
                   ];
                   array_push($questionData, $singleQuestion);
               }
               $sectionData[$section->title] = $questionData;
           }
           $data = [
               'quiz' => $sectionData,
               'title' => $quiz->title,
               'total_mark' => $quiz->total_mark,
               'per_question_time' => ($quiz->total_time * 60) / $totalQuestions
           ];
           array_push($list, $data);
        }
        return $list; 
    }

    /**
     * Get singe quiz from API request
     *
     * params: id
     * @return \Illuminate\Http\Response
     */

    public function single($id){
        $quiz = Quiz::where('id', $id)->first();
        
        $values = $quiz->questions->groupBy('section_id')->toArray();
        $totalQuestions = $quiz->questions->count();
        $data = [];
        
        $sectionData = [];
        foreach ($values as $section_id => $value) {
            $section = Section::where('id', $section_id)->select('title')->first();
            $questionData = [];
            foreach($value as $question) {
                $singleQuestion = [
                    'title' => $question['title'],
                    'options' => json_decode($question['options']),
                    'answer' => json_decode($question['answer']),
                    'type' => $question['type']
                ];
                array_push($questionData, $singleQuestion);
            }
            $sectionData[$section->title] = $questionData;
        }
        $data = [
            'quiz' => $sectionData,
            'title' => $quiz->title,
            'total_mark' => $quiz->total_mark,
            'per_question_time' => ($quiz->total_time * 60) / $totalQuestions
        ];
        return $data; 
    }
}
