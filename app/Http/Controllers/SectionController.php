<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;


class SectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::paginate(20);
       return view('dashboard.section.index', [
           'sections' => $sections
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'title' => 'required|max:255',
        ]);
        //create object and save
        $section = Section::create([
            'title' => $request->title,
        ]);
        //set flash & redirect
        if($section){
            session()->flash('success', 'Section is successfully created!'); 
            return redirect(route('section.index'));
        }
        else{
            session()->flash('error', 'Failed to create section!'); 
            return redirect(route('section.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        // dd($section->questions);
        return view('dashboard.section.show', [
            'section' => $section,
            'questions' => $section->questions,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        return view('dashboard.section.edit', [
            'section' => $section
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        //validation
        $request->validate([
            'title' => 'required|max:255',
        ]);
        //update object and save
        $section->update([
                            'title' => $request->title,
                        ]);

        //set flash & redirect
        if($section){
            session()->flash('success', 'Section is successfully updated!'); 
            return redirect(route('section.show', $section->id));
        }
        else{
            session()->flash('error', 'Failed to update section!'); 
            return redirect(route('section.edit', $section->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        //delete section
        $section->delete();
        //set flash message
        session()->flash('success', 'Section is successfully deleted!'); 
        //redirect
        return redirect(route('section.index'));
    }
}
