<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Section;
use App\Models\Question;
use Illuminate\Http\Request;


class QuizController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizess = Quiz::paginate(20);
       return view('dashboard.quiz.index', [
           'quizess' => $quizess
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.quiz.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request->validate([
            'title' => 'required|max:255',
            'total_time' => 'required|integer',
            'total_mark' => 'required|integer',
        ]);
        //create object and save
        $quiz = Quiz::create([
            'user_id' => auth()->user()->id,
            'title' => $request->title,
            'total_time' => $request->total_time,
            'total_mark' => $request->total_mark,
        ]);
        //set flash & redirect
        if($quiz){
            session()->flash('success', 'Quiz is successfully created!'); 
            return redirect(route('quiz.index'));
        }
        else{
            session()->flash('error', 'Failed to create quiz!'); 
            return redirect(route('quiz.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        
        $values = $quiz->questions->groupBy('section_id')->toArray();
        if($values){
        $totalQuestions = $quiz->questions->count();
        $data = [];
       
        $sectionData = [];
        foreach ($values as $section_id => $value) {
            $section = Section::where('id', $section_id)->select('title')->first();
            $questionData = [];
            foreach($value as $question) {
                $singleQuestion = [
                    'title' => $question['title'],
                    'options' => json_decode($question['options']),
                    'answer' => json_decode($question['answer']),
                    'type' => $question['type']
                ];
                array_push($questionData, $singleQuestion);
            }
            $sectionData[$section->title] = $questionData;
        }
        $data = [
            'quiz' => $sectionData,
            'title' => $quiz->title,
            'total_mark' => $quiz->total_mark,
            'per_question_time' => ($quiz->total_time * 60) / $totalQuestions
        ];
        
        // dd($data);
        return view('dashboard.quiz.show', [
            'quiz' => $quiz,
            'data' => $data
        ]);
        }
        else{
            return view('dashboard.quiz.show', [
                'quiz' => $quiz,
                'data' => [
                    'quiz' => [],
                    'title' => $quiz->title,
                    'total_mark' => $quiz->total_mark,
                    'per_question_time' => 0
                ]
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        return view('dashboard.quiz.edit', [
            'quiz' => $quiz
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        //validation
        $request->validate([
            'title' => 'required|max:255',
            'total_time' => 'required|integer',
            'total_mark' => 'required|integer',
        ]);
        //update object and save
        $quiz->update([
                        'title' => $request->title,
                        'total_time' => $request->total_time,
                        'total_mark' => $request->total_mark
                    ]);

        //set flash & redirect
        if($quiz){
            session()->flash('success', 'Quiz is successfully updated!'); 
            return redirect(route('quiz.show', $quiz->id));
        }
        else{
            session()->flash('error', 'Failed to update quiz!'); 
            return redirect(route('quiz.edit', $quiz->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz)
    {
        //delete quiz
        $quiz->delete();
        //set flash message
        session()->flash('success', 'Quiz is successfully deleted!'); 
        //redirect
        return redirect(route('quiz.index'));
    }
     
}
