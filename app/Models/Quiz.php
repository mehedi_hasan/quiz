<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Quiz extends Model
{
    use HasFactory;

    protected $table = 'quizess';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'total_time',
        'total_mark',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function questions()
    {
        return $this->hasMany(Question::class, 'quiz_id')->with('quiz');
    }
    public function sections()
    {
        return $this->hasManyThrough(Question::class, Section::class);
    }
}
