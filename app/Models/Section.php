<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function getTitle(Section $section){
        return $section->title;
    }

    public function questions(){
        return $this->hasMany(Question::class, 'section_id');
    }
}
