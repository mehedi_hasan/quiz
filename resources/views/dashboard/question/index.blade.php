@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Questions</h1>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            @if (count($questions) <= 0) <p>No question found.</p>
                @else
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Type</th>
                            <th scope="col">Quiz</th>
                            <th scope="col">Section</th>
                            <th scope="col">Created At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($questions as $index => $question)
                        <tr>
                            <th scope="row">{{ $index+1 }}</th>
                            <td>{{ $question->title }}</td>
                            <td>{{ $question->type }}</td>
                            <td>{{ $question->quiz->title }}</td>
                            <td>{{ $question->section->title }}</td>
                            <td>{{ date('F j, Y', strtotime($question->created_at)) }}</td>
                            <td class="d-flex">
                                <a href="{{ route('question.show', $question) }}" class="btn btn-sm btn-primary mr-2"><i
                                        class="fa fa-eye"> </i> View</a>
                                <a href="{{ route('question.edit', $question) }}" class="btn btn-sm btn-info mr-2"><i
                                        class="fa fa-pencil"> </i> Edit</a>
                                {{--  <a href="{{ route('question.destroy', $question) }}" class="btn btn-sm
                                btn-danger">Delete</a>
                                --}}
                                <form method="post" action="{{ route('question.destroy', $question) }}"
                                    class="form-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                                        onclick="return confirm('Are you sure you want to delete this item?');"> <i
                                            class="fa fa-trash"> </i> Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $questions->links() }}
                @endif
        </div>
    </div>



</div>
<!-- /.container-fluid -->
@endsection