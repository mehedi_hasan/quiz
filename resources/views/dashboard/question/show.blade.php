@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $question->title }}</h1>
        <a href="{{ route('question.edit', $question->id) }}" class="btn btn-sm btn-info shadow-sm ml-auto"><i
                class="fa fa-pencil"></i> Edit</a>
        <form method="post" action="{{ route('question.destroy', $question) }}" class="form-inline ml-2">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                onclick="return confirm('Are you sure you want to delete this item?');"> <i class="fa fa-trash"> </i>
                Delete</button>
        </form>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="question-content">
                <p><strong>Quiz:</strong> {{ $question->quiz->title }}</p>

                <p><strong>Section:</strong> {{ $question->section->title }}</p>

                <p><strong>Type:</strong> {{ $question->type }}</p>

                <p><strong>Title:</strong> {{ $question->title }}</p>

                <p><strong>Options:</strong> </p>

                <ol>
                    @foreach (json_decode($question->options) as $option)
                    <li>{{ $option }}</li>
                    @endforeach
                </ol>

                <p><strong>Correct Answer: </strong>{{ implode(',',json_decode($question->answer)) }}</p>
            </div>
        </div>
    </div>


</div>
<!-- /.container-fluid -->
@endsection