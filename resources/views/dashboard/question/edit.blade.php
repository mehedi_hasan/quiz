@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit: {{ $question->title }}</h1>
    </div>
    @if (session()->has('error'))
    <div class="alert alert-warning">
        {{ session('error') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="question-form">
                <form action="{{ route('question.update', $question->id) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Quiz</label>
                        <select class="form-control @error('quiz_id')is-invalid @enderror" id="questionSelectQuiz"
                            name="quiz_id">
                            <option>Select Quiz</option>
                            @foreach ($quizess as $quiz)
                            <option @if($question->quiz_id === $quiz->id) selected @endif
                                value="{{ $quiz->id }}">{{ $quiz->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Section</label>
                        <select class="form-control @error('section_id')is-invalid @enderror" id="questionSelectSection"
                            name="section_id">
                            <option>Select Section</option>
                            @foreach ($sections as $section)
                            <option value="{{ $section->id }}" @if($question->section_id === $section->id) selected
                                @endif>{{ $section->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="
                            questionInputTitle" name="title" value="{{ $question->title }}">
                        @error('title')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Question Type</label>
                        <select class="form-control @error('type')is-invalid @enderror" id="questionSelectType"
                            name="type" onchange="questionTypeChange(event.target.value)">
                            <option>Select Type</option>
                            <option @if($question->type === 'single') selected @endif value="single">Single</option>
                            <option @if($question->type === 'multiple') selected @endif value="multiple">Multiple
                            </option>
                        </select>
                    </div>
                    {{--  for radio & checkbox type questions  --}}

                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group option-fields">
                                <label>Options</label>
                                <input type="text" class="form-control mb-3" id="questionInputOption0" name="options[]"
                                    value="{{ $options ? $options[0] : '' }}" placeholder="Option 1">
                                <input type="text" class="form-control mb-3" id="questionInputOption1" name="options[]"
                                    value="{{ $options ? $options[1] : '' }}" placeholder="Option 2">
                                <input type="text" class="form-control mb-3" id="questionInputOption2" name="options[]"
                                    value="{{ $options ? $options[2] : '' }}" placeholder="Option 3">
                                <input type="text" class="form-control mb-3" id="questionInputOption3" name="options[]"
                                    value="{{ $options ? $options[3] : '' }}" placeholder="Option 4">
                            </div>
                        </div>
                        {{-- currect answer fields --}}
                        <div class="col-md-3">
                            <div class="form-group answer-fields">
                                <label class="d-block text-center">Currect Answer</label>
                                <input type="checkbox" class="form-control mb-3" id="questionInputAnswer0" value="1"
                                    name="answer[]" {{ in_array("1", $answer)? "checked" : "" }}>
                                <input type="checkbox" class="form-control mb-3" id="questionInputAnswer1" value="2"
                                    name="answer[]" {{ in_array("2", $answer)? "checked" : "" }}>
                                <input type="checkbox" class="form-control mb-3" id="questionInputAnswer2" value="3"
                                    name="answer[]" {{ in_array("3", $answer)? "checked" : "" }}>
                                <input type="checkbox" class="form-control mb-3" id="questionInputAnswer3" value="4"
                                    name="answer[]" {{ in_array("4", $answer)? "checked" : "" }}>

                            </div>
                        </div>
                    </div>


                    <hr>
                    <button type="submit" class="btn btn-primary">Update</button>


                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    let optionFields = $('.option-fields' );
    let answerFields = $('.answer-fields' );
    let answerInput = $('.answer-fields input' );
    let optionInput = $('.option-fields input' );
    let questionType = $('#questionSelectType' ).val();

    optionFields.hide();
    answerFields.hide();
    if(questionType === 'single'){
        optionFields.show();
        answerFields.show();
        answerInput.each(function(){
            $(this).attr('type', 'radio');
        });
    }
    else if(questionType === 'multiple'){
        optionFields.show();
        answerFields.show();
        answerInput.each(function(){
            $(this).attr('type', 'checkbox');
        });
    }else{
        optionFields.hide();
        answerFields.hide();
    } 
    const questionTypeChange = (value) => {
        if(value === 'single'){
            optionFields.show();
            answerFields.show();
            answerInput.each(function(){
                $(this).attr('type', 'radio');
            });
            answerInput.each(function(){
                $(this).removeAttr('checked');
            });
            optionInput.each(function(){
                $(this).val('');
            });
        }else if(value === 'multiple'){
            optionFields.show();
            answerFields.show();
            answerInput.each(function(){
                $(this).attr('type', 'checkbox');
            });
            answerInput.each(function(){
                $(this).removeAttr('checked');
            });
            optionInput.each(function(){
                $(this).val('');
            });
        }else{
            optionFields.hide();
            answerFields.hide();
            answerInput.each(function(){
                $(this).removeAttr('checked');
            });
            optionInput.each(function(){
                $(this).val('');
            });
        }
    }
</script>
<!-- /.container-fluid -->
@endsection