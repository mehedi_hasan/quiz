@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Quiz</h1>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            @if (count($quizess) <= 0) <p>No quiz found.</p>
                @else
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Total Time</th>
                            <th scope="col">Total Mark</th>
                            <th scope="col">Creator</th>
                            <th scope="col">Created At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($quizess as $index => $quiz)
                        <tr>
                            <th scope="row">{{ $index+1 }}</th>
                            <td>{{ $quiz->title }}</td>
                            <td>{{ $quiz->total_time }}</td>
                            <td>{{ $quiz->total_mark }}</td>
                            <td>{{ $quiz->creator->name }}</td>
                            <td>{{ date('F j, Y', strtotime($quiz->created_at)) }}</td>
                            <td class="d-flex">
                                <a href="{{ route('quiz.show', $quiz) }}" class="btn btn-sm btn-primary mr-2"><i
                                        class="fa fa-eye"></i> View</a>
                                <a href="{{ route('quiz.edit', $quiz) }}" class="btn btn-sm btn-info mr-2"><i
                                        class="fa fa-pencil"></i> Edit</a>
                                {{--  <a href="{{ route('quiz.destroy', $quiz) }}" class="btn btn-sm
                                btn-danger">Delete</a>
                                --}}
                                <form method="post" action="{{ route('quiz.destroy', $quiz) }}" class="form-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                                        onclick="return confirm('Are you sure you want to delete this item?');"><i
                                            class="fa fa-trash"> </i> Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $quizess->links() }}
                @endif
        </div>
    </div>



</div>
<!-- /.container-fluid -->
@endsection