@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Quiz: {{ $quiz->title }}</h1>
        <a href="{{ route('quiz.edit', $quiz->id) }}" class="btn btn-sm btn-info shadow-sm ml-auto"><i
                class="fa fa-pencil"></i> Edit</a>
        <form method="post" action="{{ route('quiz.destroy', $quiz) }}" class="form-inline ml-2">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                onclick="return confirm('Are you sure you want to delete this item?');"> <i class="fa fa-trash"> </i>
                Delete</button>
        </form>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="quiz-content">
                <p><strong>Total Time:</strong> {{ $quiz->total_time }} Minutes</p>
                <p><strong>Total Mark:</strong> {{ $quiz->total_mark }}</p>
                <p><strong>Per Question Time:</strong> {{ ceil($data['per_question_time']) }} sec</p>
            </div>
        </div>
    </div>

    <div class='section-content'>
        @foreach ($data['quiz'] as $section => $questions)
        <div class="section-item">
            <h5 style="background: #ddd; padding: 5px 10px;">Section: {{ $section }}</h5>
            @foreach ($questions as $question)
            <div class="question-part">
                <p><i class="fa fa-angle-double-right" aria-hidden="true"></i> {{ $question['title'] }}</p>
                <ol type='1'>
                    <li>{{ $question['options'][0] }}</li>
                    <li>{{ $question['options'][1] }}</li>
                    <li>{{ $question['options'][2] }}</li>
                    <li>{{ $question['options'][3] }}</li>
                </ol>
                <p><strong>Correct Answer: </strong>{{ implode(',',$question['answer']) }}</p>
            </div>
            @endforeach
        </div>
        @endforeach
    </div>
</div>
<!-- /.container-fluid -->
@endsection