@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit: {{ $quiz->title }}</h1>
    </div>
    @if (session()->has('error'))
    <div class="alert alert-warning">
        {{ session('error') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="quiz-form">
                <form action="{{ route('quiz.update', $quiz->id) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="
                            quizInputTitle" name="title" value="{{ $quiz->title }}">
                        @error('title')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Total Time <small>in minutes</small></label>
                        <input type="text" class="form-control @error('total_time') is-invalid @enderror"
                            id="quizInputTotalTime" name="total_time" value="{{ $quiz->total_time }}">
                        @error('total_time')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Total Mark</label>
                        <input type="text" class="form-control @error('total_mark') is-invalid @enderror"
                            id="quizInputTotalMark" name="total_mark" value="{{ $quiz->total_mark }}">
                        @error('total_mark')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection