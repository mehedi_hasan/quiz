@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $section->title }}</h1>
        <a href="{{ route('section.edit', $section->id) }}" class="btn btn-sm btn-info shadow-sm ml-auto"><i
                class="fa fa-pencil"></i> Edit</a>
        <form method="post" action="{{ route('section.destroy', $section) }}" class="form-inline ml-2">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                onclick="return confirm('Are you sure you want to delete this item?');"> <i class="fa fa-trash"> </i>
                Delete</button>
        </form>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        @foreach ($questions as $question)
        <div class="col-md-3">
            <div class="question-part">
                <p><i class="fa fa-angle-double-right" aria-hidden="true"></i> {{ $question->title }}</p>
                <ol type='1'>
                    <li>{{ json_decode($question->options)[0] }}</li>
                    <li>{{ json_decode($question->options)[1] }}</li>
                    <li>{{ json_decode($question->options)[2] }}</li>
                    <li>{{ json_decode($question->options)[3] }}</li>
                </ol>

                <p><strong>Correct Answer: </strong>{{ implode(',',json_decode($question->answer)) }}</p>
            </div>
        </div>
        @endforeach
    </div>

</div>
<!-- /.container-fluid -->
@endsection