@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit: {{ $section->title }}</h1>
    </div>
    @if (session()->has('error'))
    <div class="alert alert-warning">
        {{ session('error') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-6">
            <div class="section-form">
                <form action="{{ route('section.update', $section->id) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="
                            sectionInputTitle" name="title" value="{{ $section->title }}">
                        @error('title')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection