@extends('layouts.admin')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Section</h1>
    </div>

    @if (session()->has('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            @if (count($sections) <= 0) <p>No section found.</p>
                @else
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Created At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sections as $index => $section)
                        <tr>
                            <th scope="row">{{ $index+1 }}</th>
                            <td>{{ $section->title }}</td>
                            <td>{{ date('F j, Y', strtotime($section->created_at)) }}</td>
                            <td class="d-flex">
                                <a href="{{ route('section.show', $section) }}" class="btn btn-sm btn-primary mr-2"><i
                                        class="fa fa-eye"></i> View</a>
                                <a href="{{ route('section.edit', $section) }}" class="btn btn-sm btn-info  mr-2"><i
                                        class="fa fa-pencil"></i> Edit</a>
                                {{--  <a href="{{ route('section.destroy', $section) }}" class="btn btn-sm
                                btn-danger">Delete</a>
                                --}}
                                <form method="post" action="{{ route('section.destroy', $section) }}"
                                    class="form-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm"
                                        onclick="return confirm('Are you sure you want to delete this item?');"> <i
                                            class="fa fa-trash"> </i> Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $sections->links() }}
                @endif
        </div>
    </div>



</div>
<!-- /.container-fluid -->
@endsection